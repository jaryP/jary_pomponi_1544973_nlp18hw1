import numpy as np
import os
from sklearn.svm import LinearSVC
import pickle
import nltk
import string
import matplotlib.pyplot as plt
import itertools
import tqdm
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.ensemble import BaggingClassifier
from collections import OrderedDict

TMP_DIR = '/tmp/{}_{}_{}_{}_{}_{}_{}/'\
    .format(128, 100000, 5000000, 3, 512, 20, 2.0)

VALID_DIR = "dataset/DATA/DEV"
TRAIN_DIR = "dataset/DATA/TRAIN"
TEST_DIR = "dataset/DATA/TEST"

VECTORS_PATH = 'vectors/{}_{}_{}_{}_{}_{}_{}/'\
        .format(128, 100000, 5000000, 3, 512, 20, 2.0)

def plotConfusionMatrix(labels, prediction, labeldict):
    cm = confusion_matrix(labels, prediction)
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    print(cm)
    plt.figure()
    plt.figure(figsize=(20,20))
    plt.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
    plt.title("confusion matrix")
    plt.colorbar()
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        if cm[i,j]>0:
            plt.text(j, i, format(cm[i, j], '.2f'),
                 horizontalalignment="center",
                 color= "white" if cm[i, j] > thresh else "black", fontsize = 8  )
    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.yticks(np.arange(len(labeldict)), list(labeldict.keys()))
    plt.yticks(fontsize=12)

def  clean_data(words):
    stop = set(nltk.corpus.stopwords.words('english'))
    strip_string = string.punctuation
    com = []
    for w in words:
        w1 = w.rstrip(strip_string).lstrip(strip_string)
        if len(w1) <= 0:
            continue
        if w1 in stop and w1 != 'most':
            continue
        a = all(((ord(c) >= 65 and ord(c) <= 90) or (ord(c) >= 97 and ord(c) <= 122)) for c in w1)
        if a:
            com.append(w1)
    return com

def read_data_domain(directory, labeldict,  filename='dom'):
    domains = []
    if os.path.isfile(filename+'.pickle'):
        return pickle.load(open(filename+'.pickle', 'rb'))
    else:
        for domain in os.listdir(directory):
            label = labeldict[domain]
            print(domain, label)
            for f in os.listdir(os.path.join(directory, domain)):
                if f.endswith(".txt"):
                    with open(os.path.join(directory, domain, f), encoding="utf8") as file:
                        data = []
                        for line in file.readlines():
                            split = line.lower().strip().split()
                            data += split
                        data = clean_data(data)
                        domains.append((label, data))

        pickle.dump(domains, open(filename+'.pickle', 'wb'))
    return domains

def read_test_data(directory, filename ='test'):
    domains = []
    if os.path.isfile(filename+'.pickle'):
        return pickle.load(open(filename+'.pickle', 'rb'))
    else:
        for f in os.listdir(directory):
            if f.endswith(".txt"):
                with open(os.path.join(directory, f), encoding="utf8") as file:
                    data = []
                    for line in file.readlines():
                        split = line.lower().strip().split()
                        data += split
                    data = clean_data(data)
                    domains.append((f,data))
        pickle.dump(domains, open(filename+'.pickle', 'wb'))
    return domains

if __name__ == '__main__':

    reverse_dict = dict()
    dictionary = dict()

    with open(TMP_DIR + 'metadata.tsv', 'r', encoding="utf8") as f:
        lines = f.read().splitlines()
        for i, l in enumerate(lines):
            reverse_dict[i] = l
            dictionary[l] = i

    labeldict = OrderedDict([(d, i) for i, d in enumerate(os.listdir(TRAIN_DIR))])
    inverseLabelDict = OrderedDict(zip(labeldict.values(), labeldict.keys()))
    unk = np.load(VECTORS_PATH+'0.npy')
    EMBED_SIZE = unk.size

    emb = np.empty((len(dictionary), EMBED_SIZE))
    for i in range(len(dictionary)):
        emb[i] = np.load(VECTORS_PATH+str(i)+'.npy')

    train = read_data_domain(TRAIN_DIR, labeldict=labeldict, filename='train')
    trainFeat = []
    trainLabel = []
    documents = [" ".join(d) for l, d in train]
    tfid = TfidfVectorizer(input='content', strip_accents=None, smooth_idf=False)
    tfid.fit_transform(documents)
    tfid_dict = dict(zip(tfid.get_feature_names(), tfid.idf_))

    for l, ws in tqdm.tqdm(train):
        trainLabel.append(l)
        tot = np.zeros(EMBED_SIZE)
        totp = 0
        for w in ws:
            p = tfid_dict.get(w,0)
            p = p-1 if p >0 else 0
            totp+=p
            tot += emb[dictionary.get(w, 0)] * p
        tot /= totp
        trainFeat.append(tot)

    trainFeat = np.array(trainFeat)
    trainLabel = np.array(trainLabel)
    del train

    print('Train set created')
    svm = LinearSVC()
    clf = BaggingClassifier(svm, verbose=True, n_jobs=-1, max_samples= 0.15)
    clf.fit(trainFeat, trainLabel)
    del trainFeat
    del trainLabel

    f = read_test_data(TEST_DIR)
    testFeat = []
    fileName = []
    for name, ws in tqdm.tqdm(f):
        fileName.append(name)
        tot = np.zeros(EMBED_SIZE)
        totp = 0
        for w in ws:
            p = tfid_dict.get(w,0)
            p = p-1 if p >0 else 0
            totp+=p
            tot += emb[dictionary.get(w, 0)] * p
        tot /= totp
        testFeat.append(tot)
    testFeat = np.array(testFeat)
    print('Test set created')

    f = read_data_domain(VALID_DIR, labeldict=labeldict, filename='dev')

    devFeat = []
    devLabel = []
    for l, ws in tqdm.tqdm(f):
        devLabel.append(l)
        tot = np.zeros(EMBED_SIZE)
        totp = 0
        for w in ws:
            p = tfid_dict.get(w,0)
            p = p-1 if p >0 else 0
            totp+=p
            tot += emb[dictionary.get(w, 0)] * p
        tot /= totp
        devFeat.append(tot)
    devFeat = np.array(devFeat)
    devLabel = np.array(devLabel)
    print('Dev set created')
    del f
    del emb

    print(clf.score(devFeat, devLabel))
    print(classification_report(devLabel,clf.predict(devFeat), digits= 4))

    with open("test_answers.tsv", 'w' ,encoding="utf8") as  file:
        pred = clf.predict(testFeat)
        for name, c in zip(fileName, pred):
            name = name.replace(".txt",'')
            s = "{}\t{}\n".format(name, inverseLabelDict[c])
            file.write(s)