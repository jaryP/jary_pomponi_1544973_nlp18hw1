#%%
import os
import pickle
import tensorflow as tf
import numpy as np
import tqdm
import math
import nltk
from tensorboard.plugins import projector
from data_preprocessing import  build_dataset, save_vectors, read_analogies, build_eval_dataset, batch_creator
from evaluation import evaluation

# run on CPU
# comment this part if you want to run it on GPU
# os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"  # see issue #152
# os.environ["CUDA_VISIBLE_DEVICES"] = ""

### PARAMETERS ###

BATCH_SIZE = 512 #Number of samples per batch
EMBEDDING_SIZE = 128 # Dimension of the embedding vector.
WINDOW_SIZE = 3 # How many words to consider left and right.
NEG_SAMPLES =  64  # Number of negative examples to sample.
VOCABULARY_SIZE = 100000 #The most N word to consider in the dictionary
epochs = 20

TRAIN_DIR = "dataset/DATA/TRAIN"
VALID_DIR = "dataset/DATA/DEV"
ANALOGIES_FILE = "dataset/eval/questions-words.txt"
DOMAIN_WORDS = 5000000
LR = 2.0
path = '/tmp/{}_{}_{}_{}_{}_{}_{}/'\
        .format(EMBEDDING_SIZE, VOCABULARY_SIZE, DOMAIN_WORDS, WINDOW_SIZE, BATCH_SIZE, epochs, LR)
TMP_DIR = path
if not os.path.exists(TMP_DIR):
    os.makedirs(TMP_DIR)

### READ THE TEXT FILES ###
# Read the data into a list of strings.
# the domain_words parameters limits the number of words to be loaded per domain
def read_data(directory, domain_words=-1):
    data = []
    for domain in tqdm.tqdm(os.listdir(directory)):
    #for dirpath, dnames, fnames in os.walk(directory):
        limit = domain_words
        for f in os.listdir(os.path.join(directory, domain)):
            if f.endswith(".txt"):
                with open(os.path.join(directory, domain, f), encoding="utf8") as file:
                    for line in file.readlines():
                        split = line.lower().strip().split()
                        if limit > 0 and limit - len(split) < 0:
                            split = split[:limit]
                        else:
                            limit -= len(split)
                        if limit >= 0 or limit == -1:
                            data += split
    return data

# load the training set
if os.path.isfile('raw_data_'+str(DOMAIN_WORDS)+'.pickle'):
    raw_data = pickle.load(open('raw_data_'+str(DOMAIN_WORDS)+'.pickle','rb'))
else:
    raw_data = read_data(TRAIN_DIR, domain_words=DOMAIN_WORDS)
    pickle.dump(raw_data, open('raw_data_'+str(DOMAIN_WORDS)+'.pickle','wb'))

print('Data size', len(raw_data))

# the portion of the training set used for data evaluation
valid_size = 16  # Random set of words to evaluate similarity on.
valid_window = 100  # Only pick dev samples in the head of the distribution.
valid_examples = np.random.choice(valid_window, valid_size, replace=False)

### CREATE THE DATASET AND WORD-INT MAPPING ###

data, dictionary, reverse_dictionary = build_dataset(raw_data, VOCABULARY_SIZE)
print(len(data), len(dictionary) , len(reverse_dictionary))
del raw_data  # Hint to reduce MEMORY.
#%%
VOCABULARY_SIZE = len(dictionary)
# read the question file for the Analogical Reasoning evaluation
questions = read_analogies(ANALOGIES_FILE, dictionary)
### MODEL DEFINITION ###

graph = tf.Graph()
eval = None

with graph.as_default():
    # Define input data tensors.
    with tf.device('/cpu:0'):
            train_inputs = tf.placeholder(tf.int32, shape=[BATCH_SIZE, WINDOW_SIZE*2])
            train_labels = tf.placeholder(tf.int32, shape=[BATCH_SIZE, 1])
            valid_dataset = tf.constant(valid_examples, dtype=tf.int32)

    ### FILL HERE ###
    embeddings = tf.Variable(
				tf.random_uniform([VOCABULARY_SIZE, EMBEDDING_SIZE], -1, 1))

    with tf.name_scope('embedding'):
        # embed = tf.nn.embedding_lookup(embeddings, train_inputs)
        # embed = tf.div(tf.reduce_sum(embed, 1), skip_window*2)
        embed = tf.zeros([BATCH_SIZE, EMBEDDING_SIZE])
        for i in range(WINDOW_SIZE*2):
            embed += tf.nn.embedding_lookup(embeddings, train_inputs[:,i])
        # embed = tf.div(embed, WINDOW_SIZE*2)

    ###NN weights
    with tf.name_scope('weights'):
      weights = tf.Variable(tf.truncated_normal([VOCABULARY_SIZE, EMBEDDING_SIZE],
                         stddev=1.0 / math.sqrt(EMBEDDING_SIZE)))
    with tf.name_scope('bias'):
        bias = tf.Variable(tf.zeros([VOCABULARY_SIZE]))

    with tf.name_scope('loss'):
        loss = tf.reduce_mean(
        tf.nn.sampled_softmax_loss(
            weights=weights,
            biases=bias,
            labels=train_labels,
            inputs=embed,
            num_sampled=NEG_SAMPLES,
            num_classes=VOCABULARY_SIZE))

    tf.summary.scalar('loss', loss)

    with tf.name_scope('optimizer'):
        optimizer = tf.train.AdagradOptimizer(LR).minimize(loss)

    # Compute the cosine similarity between minibatch examples and all embeddings.
    norm = tf.sqrt(tf.reduce_sum(tf.square(embeddings), 1, keepdims=True))
    normalized_embeddings = embeddings / norm
    valid_embeddings = tf.nn.embedding_lookup(normalized_embeddings,
                                              valid_dataset)
    similarity = tf.matmul(valid_embeddings, normalized_embeddings, transpose_b=True)
    similarity = tf.identity(similarity, name="similarity")
    # Merge all summaries.
    merged = tf.summary.merge_all()

    # Add variable initializer.
    init = tf.global_variables_initializer()

    # Create a saver.
    saver = tf.train.Saver()

    # evaluation graph
    eval = evaluation(normalized_embeddings, dictionary, questions)
    print(similarity.name)
### TRAINING ###
#%%
# Step 5: Begin training.
num_steps = 200001
batch_creator = batch_creator(data=data, size=BATCH_SIZE, winsize=WINDOW_SIZE)
num_steps = batch_creator.ln//batch_creator.size
ev = 0

del data

with tf.Session(graph=graph) as session:
    # Open a writer to write summaries.
    writer = tf.summary.FileWriter(TMP_DIR, session.graph)
    # We must initialize all variables before we use them.
    init.run()
    print('Initialized')

    average_loss = 0
    bar = tqdm.tqdm(range(num_steps*epochs))
    ev = eval.eval(session)
    for step in bar:
        # batch_inputs, batch_labels = gb(data, BATCH_SIZE, 1, WINDOW_SIZE)
        batch_inputs, batch_labels = batch_creator.getBatch(step)

        # Define metadata variable.
        run_metadata = tf.RunMetadata()

        # We perform one update step by evaluating the optimizer op
        _, summary, loss_val = session.run(
            [optimizer, merged, loss],
            feed_dict={train_inputs: batch_inputs, train_labels: batch_labels},
            run_metadata=run_metadata)
        average_loss += loss_val
        del batch_inputs
        del batch_labels
        # Add returned summaries to writer in each step.
        writer.add_summary(summary, step)
        # Add metadata to visualize the graph for the last run.

        if step % 10000 == 0:
            sim = similarity.eval()
            for i in range(valid_size):
                valid_word = reverse_dictionary[valid_examples[i]]
                top_k = 8  # number of nearest neighbors
                nearest = (-sim[i, :]).argsort()[1:top_k + 1]
                log_str = 'Nearest to %s:' % valid_word
                for k in range(top_k):
                    close_word = reverse_dictionary[nearest[k]]
                    log_str = '%s %s,' % (log_str, close_word)
                print(log_str)
        if step == (num_steps - 1):
            writer.add_run_metadata(run_metadata, 'step%d' % step)
        if step % 10000 is 0:
            ev = eval.eval(session)
            print("avg loss: "+str(average_loss/(step+1)))

    words_report = ['cat', 'german', 'food', 'most', 'general', 'eat', 'teach']
    words_report_index = [ dictionary.get(w,0) for w in words_report]
    s = '\nEmbed size: {}, Dict size: {}, Dom words: {}, Win size: {}, Batch size: {}, Steps (epochs): {} ({}), LR: {}\n'\
        .format(EMBEDDING_SIZE, VOCABULARY_SIZE, DOMAIN_WORDS, WINDOW_SIZE, BATCH_SIZE, num_steps, epochs, LR)
    s+= ev +'\n'

    valid_embeddings = tf.nn.embedding_lookup(normalized_embeddings,
                                              words_report_index)
    similarity = tf.matmul(valid_embeddings, normalized_embeddings, transpose_b=True)
    similarity = tf.identity(similarity, name="similarity")
    sim = similarity.eval()

    for i in range(len(words_report)):
        s += 'nearest to ' + words_report[i] +' ('+str(words_report_index[i])+'): '
        nearest = (-sim[i, :]).argsort()[1:5 + 1]
        for n in nearest:
            s += reverse_dictionary[n]+', '
        s+='\n'
    print(s)

    with open('results.txt', mode='a', encoding="utf8") as f:
        f.write(s)

    final_embeddings = normalized_embeddings.eval()

    ### SAVE VECTORS ###

    path = 'vectors/{}_{}_{}_{}_{}_{}_{}/'\
        .format(EMBEDDING_SIZE, VOCABULARY_SIZE, DOMAIN_WORDS, WINDOW_SIZE, BATCH_SIZE, epochs, LR)
    save_vectors(final_embeddings, folder_save=path)

    # Write corresponding labels for the embeddings.
    with open(TMP_DIR + 'metadata.tsv', 'w', encoding="utf8") as f:
        for i in range(VOCABULARY_SIZE):
            f.write(reverse_dictionary[i] + '\n')

    # Save the model for checkpoints
    saver.save(session, os.path.join(TMP_DIR, 'model.ckpt'))

    # Create a configuration for visualizing embeddings with the labels in TensorBoard.
    config = projector.ProjectorConfig()
    embedding_conf = config.embeddings.add()
    embedding_conf.tensor_name = embeddings.name
    embedding_conf.metadata_path = os.path.join(TMP_DIR, 'metadata.tsv')
    projector.visualize_embeddings(writer, config)

writer.close()
