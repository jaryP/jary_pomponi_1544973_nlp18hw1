import collections

import tqdm
import numpy as np
from sklearn.utils import shuffle
import string
import nltk
import os
import tensorflow as tf

def build_eval_dataset(data, dictionary):
    com = []
    for w in data:
        if w in dictionary:
            com.append(dictionary[w])
        else:
            com.append(0)
    return com

###batch_creator###
#this class is used to generate the batches
###Parameters###
# data: the dataset
# size: the number of train_data,label pairs to produce per batch
# window_size: the size of the context

class batch_creator:
    def __init__(self, data, size, winsize):
            self.size = size
            self._index = 0

            indexes = np.concatenate((range(-winsize, 0), range(1, winsize + 1)))
            batches = []
            labels = []
            i = winsize
            for _ in tqdm.tqdm(data):
                if i + winsize >= len(data):
                    break
                c = []
                for s in indexes:
                    c.append(data[i+s])
                batches.append(c)
                labels.append(data[i])
                i += 1
            self.labels = np.array(labels).reshape((len(labels), 1))
            self.batches = np.array(batches)
            self.batches, self.labels = shuffle(self.batches, self.labels)
            self.ln = len(batches)

    def getBatch(self, step):

        start = (step * self.size) % self.ln
        end = (start + self.size)

        if end > self.ln:
            start = 0
            end = self.size
            self.batches, self.labels = shuffle(self.batches, self.labels)
        data = self.batches[start:end]
        label = self.labels[start:end]

        return data, label


### build_dataset ###
# This function is responsible of generating the dataset and dictionaries.
# While constructing the dictionary take into account the unseen words by
# retaining the rare (less frequent) words of the dataset from the dictionary
# and assigning to them a special token in the dictionary: UNK. This
# will train the model to handle the unseen words.
### Parameters ###
# words: a list of words
# vocab_size:  the size of vocabulary
#
### Return values ###
# data: list of codes (integers from 0 to vocabulary_size-1).
#       This is the original text but words are replaced by their codes
# dictionary: map of words(strings) to their codes(integers)
# reverse_dictionary: maps codes(integers) to words(strings)


def build_dataset(words, vocab_size):
    dictionary = dict()
    stop = set(nltk.corpus.stopwords.words('english'))
    data = []
    strip_string = string.punctuation
    com = []
    for w in words:
        w1 = w.rstrip(strip_string).lstrip(strip_string)
        if len(w1) <= 0:
            continue
        if w1 in stop and w1 != 'most':
            continue
        #Only alphabetic character are allowed
        a = all(((ord(c) >= 65 and ord(c) <= 90) or (ord(c) >= 97 and ord(c) <= 122)) for c in w1)
        if a:
            com.append(w1)
    words = com
    del com
    unk = 0
    dictionary['UNK'] = unk

    for w, _ in collections.Counter(words).most_common(vocab_size-1):
        dictionary[w] = len(dictionary)

    for w in tqdm.tqdm(words):
        index = dictionary.get(w, unk)
        data.append(index)

    reversed_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
    return data, dictionary, reversed_dictionary


###
# Save embedding vectors in a suitable representation for the domain identification task
###
def save_vectors(vectors, folder_save = './vectors/'):
    if not os.path.exists(folder_save):
        os.makedirs(folder_save)
    for i, v in enumerate(vectors):
        np.save(folder_save+str(i), v)


# Reads through the analogy question file.
#    Returns:
#      questions: a [n, 4] numpy array containing the analogy question's
#                 word ids.
#      questions_skipped: questions skipped due to unknown words.
#
def read_analogies(file, dictionary):
    questions = []
    questions_skipped = 0
    with open(file, "r") as analogy_f:
        for line in analogy_f:
            if line.startswith(":"):  # Skip comments.
                continue
            words = line.strip().lower().split(" ")
            ids = [dictionary.get(str(w.strip())) for w in words]
            if None in ids or len(ids) != 4:
                questions_skipped += 1
            else:
                questions.append(np.array(ids))

    print("Eval analogy file: ", file)
    print("Questions: ", len(questions))
    print("Skipped: ", questions_skipped)
    return np.array(questions, dtype=np.int32)